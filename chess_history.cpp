#include "chess_history.h"

ChessHistory::ChessHistory()
    : m_current_move{ m_moves.begin() }
{
}

ChessHistory::ChessHistory(const ChessGrid &input_board, const QList<ChessMove> input_moves)
    : m_initial_board{ input_board }
    , m_moves{ input_moves }
    , m_current_move{ m_moves.begin() }
{
}

void ChessHistory::set_initial_board(const ChessGrid &chessboard)
{
    m_initial_board = chessboard;
    clear_history();
}

void ChessHistory::add_move(const ChessMove &move)
{
    m_moves.append( move );
}

std::shared_ptr<ChessMove> ChessHistory::next_move()
{
    return is_last() ? nullptr
                : std::make_shared<ChessMove>( *(m_current_move++) );
}

std::shared_ptr<ChessMove> ChessHistory::prev_move()
{
    return is_first() ? nullptr
                : std::make_shared<ChessMove>( *(--m_current_move) );
}

bool ChessHistory::is_last() const
{
    return m_current_move == m_moves.end();
}

bool ChessHistory::is_first() const
{
    return m_current_move == m_moves.begin();
}

void ChessHistory::clear_history()
{
    m_moves.clear();
    m_current_move = m_moves.begin();
}

ChessGrid ChessHistory::initial_board() const
{
    return m_initial_board;
}

QList<ChessMove> ChessHistory::moves() const
{
    return m_moves;
}

QDataStream& operator<<( QDataStream& out, const ChessHistory& chess_history  )
{
    out << static_cast<int>(chess_history.initial_board().size());
    for( const auto& element : chess_history.initial_board() )
        out << element;
    out << chess_history.moves();
    return out;
}

QDataStream& operator>>( QDataStream& in, ChessHistory& chess_history )
{
    ChessGrid initial_board;
    QList<ChessMove> moves;
    int board_size;
    in >> board_size;
    for( int count=0; count < board_size; ++count )
    {
        Chessman chessman;
        in >> chessman;
        initial_board.push_back( chessman );
    }
    in >> moves;
    chess_history = ChessHistory( initial_board, moves );
    return in;
}
