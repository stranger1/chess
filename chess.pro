TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    model_chessboard.cpp \
    chess_history.cpp \
    chessman.cpp \
    chess_move.cpp \
    chessgrid.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    model_chessboard.h \
    chessman.h \
    chess_history.h \
    chess_move.h \
    chessgrid.h

CONFIG += c++11

DISTFILES +=
