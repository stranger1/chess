#include "model_chessboard.h"

#include <QColor>
#include <QDebug>
#include <QFile>
#include <QUrl>

ModelChessboard::ModelChessboard( QObject *parent )
    : QAbstractListModel{ parent }
    , cells{ create_chessgrid( ChessSetup::EMPTY ) }
    , m_mode{ GameMode::GAME }
{
}

ModelChessboard::ModelChessboard( const ChessGrid &chess_grid , QObject* parent )
    : QAbstractListModel{ parent }
    , cells{ chess_grid }
    , m_active_cell{ NONE_CELL }
    , m_mode{ GameMode::REPLAY }
{
}

int ModelChessboard::rowCount( const QModelIndex& ) const
{
    return GRID_DIMENSION * GRID_DIMENSION;
}

int ModelChessboard::columnCount( const QModelIndex& ) const
{
    return GRID_DIMENSION;
}

QVariant ModelChessboard::data( const QModelIndex &index, int role ) const
{
    if ( !index.isValid() || index.row() < 0 || index.row() >= GRID_DIMENSION*GRID_DIMENSION )
    {
        return QVariant();
    }
    QVariant result;
    switch ( role ) {
        case DataRoles::ChessmanRole:
        {
            const QString cell{ chessman_name[cells[index.row()].title] };
            result = QVariant( cell );
            break;
        }
        case DataRoles::ColourRole:
        {
            const QColor colour = cells[index.row()].colour == Colour::WHITE ? QColor{255,255,255} //TODO: make it actually white
                                                                               : QColor{0,0,0};
            result = QVariant( colour );
            break;
        }
        default:
            break;
    }

    return result;
}

QHash<int, QByteArray> ModelChessboard::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DataRoles::ChessmanRole] = "ChessmanName";
    roles[DataRoles::ColourRole] = "Colour";

    return roles;
}

Chessman ModelChessboard::get_cell( const int letter, const int number ) const
{
    return cells[ coord_to_index (letter, number) ];
}

bool ModelChessboard::save_replay( const QString& file_url ) const
{
    QFile file{ QUrl{file_url}.toLocalFile() };
    try {
        file.open( QIODevice::WriteOnly );
        QDataStream stream( &file );
        stream << m_history;
        file.close();
    } catch(...) {
        if( file.isOpen() )
            file.close();
        return false;
    }
    return true;
}

bool ModelChessboard::load_replay(const QString& file_url)
{
    QFile file{ QUrl{file_url}.toLocalFile() };
    ChessHistory loaded_history;
    try {
        file.open( QIODevice::ReadOnly );
        QDataStream stream( &file );
        stream >> loaded_history;
        file.close();
    } catch(...) {
        if( file.isOpen() )
            file.close();
        return false;
    }
    std::swap( loaded_history, m_history );
    this->beginResetModel();
    cells = m_history.initial_board();
    m_active_cell = NONE_CELL;
    this->endResetModel();
    return true;
}

void ModelChessboard::load_empty_chessboard()
{
    load_chessboard( create_chessgrid(ChessSetup::EMPTY) );
}

void ModelChessboard::load_default_chess_placement()
{
    load_chessboard( create_chessgrid(ChessSetup::STANDARD) );
}

bool ModelChessboard::prev_move()
{
    const auto move = m_history.prev_move();
    if( move ) move_chessman( move->to_index, move->from_index );
    return !m_history.is_first();
}

bool ModelChessboard::next_move()
{
    const auto move = m_history.next_move();
    if( move ) move_chessman( move->from_index, move->to_index );
    return !m_history.is_last();
}

bool ModelChessboard::set_active_cell( const int index )
{
    if( index == NONE_CELL )
    {
        //no active cell selected
        m_active_cell = index;
        emit active_cell_changed();
        return true;
    }
    Chessman cell( cells[index] );
    if( cell.title == Chessman::EMPTY )
        return false;
    if( cell.colour != m_active_player ) //selecting chessman of an opponent
        return false;
    m_active_cell = index;
    emit active_cell_changed();
    return true;
}

int ModelChessboard::active_cell() const
{
    return m_active_cell;
}

QString ModelChessboard::active_player() const
{
    if( m_active_player == Colour::WHITE )
        return QString{"Player 1 (White)"};
    else
        return QString{"Player 2 (Black)"};
}

bool ModelChessboard::move_chessman( const int from_index, const int to_index )
{
    Chessman source_cell( cells[from_index] );
    Chessman dest_cell( cells[to_index] );
    if ( source_cell.title == Chessman::EMPTY || source_cell.colour == dest_cell.colour )
        return false;
    this->beginResetModel();
    cells[to_index] = cells[from_index];
    cells[from_index] = Chessman{ Chessman::EMPTY, Colour::EMPTY };
    qDebug() << "Moved from "  << from_index << " to " << to_index;
    set_active_cell( -1 );
    this->endResetModel();
    return true;
}

bool ModelChessboard::move_active_chessman( const int dest_index )
{
    if( cells[m_active_cell].colour == cells[dest_index].colour ) {
        set_active_cell( dest_index );
        return false;
    }
    if( p_is_move_allowed( m_active_cell, dest_index ) )
    {
        this->beginResetModel();
        cells[dest_index] = cells[m_active_cell];
        cells[m_active_cell] = Chessman{ Chessman::EMPTY, Colour::EMPTY };
        m_history.add_move({m_active_cell, dest_index});
        set_active_cell( NONE_CELL );
        this->endResetModel();
        p_change_player();
        return true;
    }
    return false;
}

void ModelChessboard::load_chessboard( const ChessGrid &chessboard )
{
    this->beginResetModel();
    cells = chessboard;
    m_active_cell = NONE_CELL;
    m_history.set_initial_board( cells );
    m_active_player = Colour::WHITE;
    this->endResetModel();
}

bool ModelChessboard::p_is_move_allowed( const int from_index, const int to_index ) const
{
    Chessman from_cell( cells[from_index] );
    Chessman to_cell( cells[to_index] );
    if( from_cell.colour == to_cell.colour )
        return false;
    //Chess rules
    const int d_number{ delta_number( from_index, to_index ) };
    const int d_letter{ delta_letter( from_index, to_index ) };
    bool result{ false };
    switch( from_cell.title )
    {
    case Chessman::PAWN:
    {
        int from_number{ index_to_coord(from_index).first };
        int to_number{ index_to_coord(to_index).first };
        //Check whether move direction is correct
        if( from_number < to_number )
        {
            if( from_cell.colour == Colour::BLACK )
                break;
        }
        else
        {
            if( from_cell.colour == Colour::WHITE )
                break;
        }

        //Is attack performed
        if( d_number == 1 && d_letter == 1
          && to_cell.title != Chessman::EMPTY )
        {
                result = true;
                break;
        }

        if( d_letter != 0 ) //Pawn moves only vertically
            break;
        //Initial 2-square move check
        if( from_cell.colour == Colour::WHITE )
        {
            if( from_number == 1 && d_number == 2 )
            {
                result = true;
                break;
            }
        }
        else if( from_number == 6 && d_number == 2 )
        {
            result = true;
            break;
        }
        //Simple move
        if( d_number == 1 )
        {
            result = true;
            break;
        }
        break;
    }
    case Chessman::KNIGHT:
        if(  (d_letter == 2 && d_number == 1)
          || (d_letter == 1 && d_number == 2)  )
            result = true;
        break;
    case Chessman::BISHOP:
        if( d_letter == d_number )
            result = true;
        break;
    case Chessman::ROOK:
        if( d_letter == 0 || d_number == 0 )
            result = true;
        break;
    case Chessman::QUEEN:
        if( d_letter == d_number )
            result = true;
        if( d_letter == 0 || d_number == 0 )
            result = true;
        break;
    case Chessman::KING:
        if( d_letter <= 1 && d_number <= 1 )
            result = true;
        break;
    default:
        break;
    }
    if( result )
        result = p_is_way_clear( from_index, to_index, d_letter, d_number );
    return result;
}

bool ModelChessboard::p_is_way_clear( const int from_index, const int to_index
                                    , const int d_letter, const int d_number ) const
{
    Chessman chessman( cells[from_index] );
    if( chessman.title == Chessman::KNIGHT )
        return true;
    if( chessman.title == Chessman::PAWN )
    {
        if( d_letter == 0 && cells[to_index].title != Chessman::EMPTY )
            return false;
        if( d_number == 2 )
        {
            if( chessman.colour == Colour::WHITE )
                //indexes grow in the direction opposite to the numbers
                //on actual chessboard
                return cells[from_index - GRID_DIMENSION].title == Chessman::EMPTY;
            else
                return cells[from_index + GRID_DIMENSION].title == Chessman::EMPTY;
        }
    }
    if( (d_letter <= 1) && (d_number <= 1) )
        return true;

    bool clear_way{ true };
    {
        const auto from_number_letter = index_to_coord(from_index);
        const auto to_number_letter = index_to_coord(to_index);
        const int from_number{ from_number_letter.first };
        const int to_number{ to_number_letter.first };
        const int from_letter{ from_number_letter.second };
        const int to_letter{ to_number_letter.second };

        //TODO: refactor straight movement
        if( d_letter == 0 )
        {
            //Vertical move
            int j, end;
            if( from_number < to_number )
            {
                j = from_number+1; end = to_number;
            }
            else
            {
                j = to_number+1; end = from_number;
            }
            for( ; j<end; ++j )
            {
                if( get_cell(from_letter, j).title != Chessman::EMPTY )
                    clear_way = false;
            }
        }
        else if( d_number == 0 )
        {
            //Horizontal move
            int i, end;
            if( from_letter < to_letter )
            {
                i = from_letter+1; end = to_letter;
            }
            else
            {
                i = to_letter+1; end = from_letter;
            }
            for( ; i<end; ++i )
            {
                if( get_cell(i, from_number).title != Chessman::EMPTY )
                    clear_way = false;
            }
        }
        else
        {
            //Diagonal move
            int i_inc, j_inc; //Movement direction
            if( from_letter < to_letter )
                i_inc = 1;
            else
                i_inc = -1;
            if( from_number < to_number )
                j_inc = 1;
            else
                j_inc = -1;

            int i = from_letter+i_inc;
            int j = from_number+j_inc;
            for( int counter = 0; counter < d_letter-1; ++counter )
            {
                if( get_cell(i, j).title != Chessman::EMPTY )
                {
                    clear_way = false;
                    break;
                }
                i += i_inc;
                j += j_inc;
            }
        }

    }
    return clear_way;
}

void ModelChessboard::p_change_player()
{
    m_active_player = next_player( m_active_player );
    emit active_player_changed();
}
