#include "chess_move.h"

QDataStream& operator<<( QDataStream& out, const ChessMove& move )
{
    out << move.from_index << move.to_index;
    return out;
}

QDataStream& operator>>( QDataStream& in, ChessMove& move )
{
    in >> move.from_index >> move.to_index;
    return in;
}
