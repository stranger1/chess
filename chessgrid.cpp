#include "chessgrid.h"

Chessman set_cell( ChessGrid& chessgrid, const int letter, const int number, Chessman chessman )
{
    int number_index{ GRID_DIMENSION - number - 1 };
    int index = number_index * GRID_DIMENSION + letter;
    std::swap( chessgrid[index], chessman );
    return chessman;
}

int coord_to_index( const int letter, const int number )
{
    // On the view numeration starts from the top, but on the chessboard it starts from the bottom
    int number_index{ GRID_DIMENSION - number - 1 };
    return number_index * GRID_DIMENSION + letter;
}

QPair<int, int> index_to_coord( const int index )
{
    QPair<int, int> result;
    int number_index = index / GRID_DIMENSION;
    result.first = GRID_DIMENSION - 1 - number_index;
    result.second = index - number_index * GRID_DIMENSION;
    return result;
}

int delta_number( const int from_index, const int to_index )
{
    int from_number{ index_to_coord(from_index).first };
    int to_number{ index_to_coord(to_index).first };
    return qAbs( from_number - to_number );
}

int delta_letter( const int from_index, const int to_index )
{
    int from_letter{ index_to_coord(from_index).second };
    int to_letter{ index_to_coord(to_index).second };
    return qAbs( from_letter - to_letter );
}

ChessGrid create_chessgrid(ChessSetup setup)
{
    ChessGrid chessgrid( GRID_DIMENSION*GRID_DIMENSION, Chessman{ Chessman::EMPTY, Colour::EMPTY } );
    switch( setup )
    {
    case ChessSetup::STANDARD:
    {
        set_cell( chessgrid,  0, 7, Chessman{ Chessman::ROOK, Colour::BLACK } );
        set_cell( chessgrid,  1, 7, Chessman{ Chessman::KNIGHT, Colour::BLACK } );
        set_cell( chessgrid,  2, 7, Chessman{ Chessman::BISHOP, Colour::BLACK } );
        set_cell( chessgrid,  3, 7, Chessman{ Chessman::KING, Colour::BLACK } );
        set_cell( chessgrid,  4, 7, Chessman{ Chessman::QUEEN, Colour::BLACK } );
        set_cell( chessgrid,  5, 7, Chessman{ Chessman::BISHOP, Colour::BLACK } );
        set_cell( chessgrid,  6, 7, Chessman{ Chessman::KNIGHT, Colour::BLACK } );
        set_cell( chessgrid,  7, 7, Chessman{ Chessman::ROOK, Colour::BLACK } );

        set_cell( chessgrid,  0, 0, Chessman{ Chessman::ROOK, Colour::WHITE } );
        set_cell( chessgrid,  1, 0, Chessman{ Chessman::KNIGHT, Colour::WHITE } );
        set_cell( chessgrid,  2, 0, Chessman{ Chessman::BISHOP, Colour::WHITE } );
        set_cell( chessgrid,  3, 0, Chessman{ Chessman::QUEEN, Colour::WHITE } );
        set_cell( chessgrid,  4, 0, Chessman{ Chessman::KING, Colour::WHITE } );
        set_cell( chessgrid,  5, 0, Chessman{ Chessman::BISHOP, Colour::WHITE } );
        set_cell( chessgrid,  6, 0, Chessman{ Chessman::KNIGHT, Colour::WHITE } );
        set_cell( chessgrid,  7, 0, Chessman{ Chessman::ROOK, Colour::WHITE } );

        int current_cell{GRID_DIMENSION};
        while ( current_cell-- ) {
            set_cell( chessgrid,  current_cell, 6, Chessman{ Chessman::PAWN, Colour::BLACK } );
            set_cell( chessgrid,  current_cell, 1, Chessman{ Chessman::PAWN, Colour::WHITE } );
        }
        break;
    }
    case ChessSetup::EMPTY:
    default:
        break;
    }
    return chessgrid;
}
