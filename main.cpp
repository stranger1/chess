#include "model_chessboard.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    ModelChessboard model_chessboard;
    engine.rootContext()->setContextProperty( "model_chessboard", &model_chessboard );

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
    return app.exec();
}
