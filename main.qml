import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    minimumWidth: 600
    maximumWidth: minimumWidth
    minimumHeight: 600
    maximumHeight: minimumHeight

    title: qsTr("Chess")

    Column {
        id: mainScreen
        state: "MAIN_MENU"

        states: [
            State {
                name: "MAIN_MENU"
                PropertyChanges { target: chessBoard; state: "INACTIVE" }
                PropertyChanges { target: buttonsMenu; buttonStartVisible: true }
                PropertyChanges { target: buttonsMenu; buttonStopVisible: false }
                PropertyChanges { target: buttonsMenu; buttonLoadVisible: true }
                PropertyChanges { target: buttonsMenu; buttonSaveVisible: false }
                PropertyChanges { target: replayNavigationButtons; visible: false }
                PropertyChanges { target: labelCurrentPlayer; visible: false }
                StateChangeScript {
                    script: model_chessboard.load_empty_chessboard();
                }
            },
            State {
                name: "GAME"
                PropertyChanges { target: chessBoard; state: "ACTIVE" }
                PropertyChanges { target: buttonsMenu; buttonStartVisible: false }
                PropertyChanges { target: buttonsMenu; buttonStopVisible: true }
                PropertyChanges { target: buttonsMenu; buttonLoadVisible: true }
                PropertyChanges { target: buttonsMenu; buttonSaveVisible: true }
                PropertyChanges { target: replayNavigationButtons; visible: false }
                PropertyChanges { target: labelCurrentPlayer; visible: true }
                StateChangeScript {
                    script: model_chessboard.load_default_chess_placement();
                }
            },
            State {
                name: "REPLAY"
                PropertyChanges { target: chessBoard; state: "INACTIVE" }
                PropertyChanges { target: buttonsMenu; buttonStartVisible: true }
                PropertyChanges { target: buttonsMenu; buttonStopVisible: false }
                PropertyChanges { target: buttonsMenu; buttonLoadVisible: true }
                PropertyChanges { target: buttonsMenu; buttonSaveVisible: false }
                PropertyChanges { target: replayNavigationButtons; visible: true }
                PropertyChanges { target: labelCurrentPlayer; visible: false }
            }

        ]

        MainMenu {
            id: buttonsMenu
        }
        Item {
            width: 480
            height: 480

            Grid {
                id: chessBoard
                columns: 8
                spacing: 0
                x: 0
                y: 0
                rows: 8
                state: "INACTIVE"

                states: [
                    State {
                        name: "INACTIVE"
                    },
                    State {
                        name: "ACTIVE"
                    }

                ]

                width: parent.width
                height: parent.height

                property int sourceCell: -1
                property int destCell: -1
                Repeater {
                    id: view_chessboard
                    model: model_chessboard

                    Rectangle {
                        width: 60
                        height: 60
                        border {
                            color: "green"
                            width: index == model_chessboard.active_cell ? 5 : 0
                        }

                        color: ( (Math.floor(index / 8) % 2) === 0 )
                               ? (index % 2 === 1 ? "#D18B47" : "#FFCE9E") // light wood
                               : (index % 2 === 0 ? "#D18B47" : "#FFCE9E") // dark wood
                        Text {
                            id: cellText
                            text: ChessmanName
                            color: Colour
                            font.pointSize: 16
                            font.bold: true
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if ( chessBoard.state === "INACTIVE" )
                                    return;
                                if ( model_chessboard.active_cell === -1 ){
                                    model_chessboard.active_cell = index;
                                } else {
                                    model_chessboard.move_active_chessman(index);
                                }
                            }
                        }
                    }
                }
            }
        }
        Row {
            // Buttons for navigating during replay
            id: replayNavigationButtons
            spacing: chessBoard.width - buttonPrevMove.width - buttonNextMove.width
            Button {
                id: buttonPrevMove
                enabled: false
                text: "<<"
                onClicked: {
                    if( !model_chessboard.prev_move() )
                        buttonPrevMove.enabled = false
                    else if( !buttonNextMove.enabled )
                        buttonNextMove.enabled = true
                }
            }
            Button {
                id: buttonNextMove
                text: ">>"
                onClicked: {
                    if( !model_chessboard.next_move() )
                        buttonNextMove.enabled = false
                    else if( !buttonPrevMove.enabled )
                        buttonPrevMove.enabled = true
                }
            }
        }
        Text {
            id: labelCurrentPlayer
            text: model_chessboard.active_player
            font.pointSize: 14
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}

