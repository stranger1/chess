import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.1

Row {
    // Main menu buttons
    spacing: 5

    signal saveFileChosen
    signal openFileChosen

    property alias saveFilePath: saveFileDialog.fileUrl
    property alias openFilePath: openFileDialog.fileUrl

    property bool buttonStartVisible: true
    property bool buttonStopVisible: true
    property bool buttonLoadVisible: true
    property bool buttonSaveVisible: true

    FileDialog {
        id: saveFileDialog
        title: "Please choose path to save file"
        selectExisting: false

        onAccepted: {
            console.log("You chose: " + saveFileDialog.fileUrl)
            buttonsMenu.saveFileChosen();
        }
        onRejected: {
            console.log("Canceled")
            close()
        }
    }

    FileDialog {
        id: openFileDialog
        title: "Please choose file to load a game from"

        onAccepted: {
            console.log( "You chose " + openFileDialog.fileUrl )
            buttonsMenu.openFileChosen()
        }
        onRejected: {
            close()
        }
    }

    MessageDialog {
        id: messageDialog
        onAccepted: close()
    }

    Button {
        id: buttonStart
        text: "Start"
        visible: buttonStartVisible
        onClicked: {
            mainScreen.state = "GAME";
        }
    }
    Button {
        id: buttonStop
        text: "Stop"
        visible: buttonStopVisible
        onClicked: {
            mainScreen.state = "MAIN_MENU"
            model_chessboard.load_empty_chessboard();
        }
    }
    Button {
        id: buttonLoad
        text: "Load"
        visible: buttonLoadVisible
        onClicked: {
            openFileDialog.open()
        }
    }
    Button {
        id: buttonSave
        text: "Save"
        visible: buttonSaveVisible
        onClicked: {
            saveFileDialog.open()
        }
    }
    onSaveFileChosen: {
        if( !model_chessboard.save_replay( saveFilePath ) ) {
            messageDialog.title = qsTr( "Failed to open file" )
            messageDialog.text = qsTr( "Failed to save to file " + saveFilePath )
            messageDialog.open()
            return
        }
    }
    onOpenFileChosen: {
        if( !model_chessboard.load_replay( openFilePath ) ) {
            messageDialog.title = qsTr( "Failed to open file" )
            messageDialog.text = qsTr( "Failed to open file " + openFilePath
                                     + ".\n File has wrong format or you don't have permission to read it.")
            messageDialog.open()
            return
        }
        mainScreen.state = "REPLAY"
    }
}
