#ifndef MODEL_CHESSBOARD_H
#define MODEL_CHESSBOARD_H

#include "chessman.h"
#include "chess_history.h"
#include "chessgrid.h"

#include <QAbstractListModel>

class ModelChessboard : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY( int active_cell READ active_cell WRITE set_active_cell NOTIFY active_cell_changed )
    Q_PROPERTY( QString active_player READ active_player NOTIFY active_player_changed )
public:
    explicit ModelChessboard( QObject *parent = 0 );
    explicit ModelChessboard( const ChessGrid& chess_grid, QObject *parent = 0 );

    int rowCount( const QModelIndex &parent = QModelIndex{} ) const override;
    int columnCount( const QModelIndex &parent = QModelIndex{} ) const override;
    QVariant data( const QModelIndex &index, int role ) const override;
    QHash<int, QByteArray> roleNames() const override;

    enum DataRoles
    {
        // Roles for QAbstractListModel
        ChessmanRole = Qt::UserRole + 1
        , ColourRole
    };

    enum class GameMode
    {
        GAME
        , REPLAY
    };

    Q_INVOKABLE Chessman get_cell( const int letter, const int number ) const;
    Q_INVOKABLE bool move_active_chessman( int dest_index );
    Q_INVOKABLE void load_empty_chessboard();
    Q_INVOKABLE void load_default_chess_placement();
    Q_INVOKABLE bool save_replay( const QString& file_url ) const;
    Q_INVOKABLE bool load_replay(const QString& file_url );
    /*! \return \c false - if the previous move is the first */
    Q_INVOKABLE bool prev_move();
    /*! \return \c false - if the next move is the last */
    Q_INVOKABLE bool next_move();

    void load_chessboard( const ChessGrid& chessboard );
    bool move_chessman( const int from_index, const int to_index );
    int active_cell() const;
    bool set_active_cell( const int index );
    QString active_player() const;

private:
    ChessGrid cells;
    static constexpr int NONE_CELL = -1;

    int m_active_cell;
    Colour m_active_player;
    GameMode m_mode;
    ChessHistory m_history;

    bool p_is_move_allowed( const int from_index, const int to_index ) const;
    bool p_is_way_clear(const int from_index, const int to_index
                       , const int d_letter, const int d_number ) const;
    void p_change_player();
signals:
    void active_cell_changed();
    void active_player_changed();
public slots:
};

#endif // MODEL_CHESSBOARD_H
