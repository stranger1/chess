#include "chessman.h"

#include <exception>

QDataStream& operator<<( QDataStream& out, const Chessman& chessman )
{
    out << chessman.title << static_cast<int>(chessman.colour);
    return out;
}

QDataStream& operator>>( QDataStream& in, Chessman& chessman )
{
    int colour_int, title_int;
    in >> title_int >> colour_int;
    if( (title_int >= Chessman::COUNT)
      || (static_cast<Colour>(colour_int) >= Colour::COUNT) )
        throw std::runtime_error( "Wrong serialised Chessman format" );
    chessman.title = static_cast<Chessman::type>(title_int);
    chessman.colour = static_cast<Colour>( colour_int );
    return in;
}
