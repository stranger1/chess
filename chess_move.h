#ifndef CHESS_MOVE
#define CHESS_MOVE

#include "chessman.h"

#include <QDataStream>

struct ChessMove
{
    int from_index;
    int to_index;
};

QDataStream& operator<<( QDataStream& out, const ChessMove& move );
QDataStream& operator>>( QDataStream& in, ChessMove& move );

#endif // CHESS_MOVE

