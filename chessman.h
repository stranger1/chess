#ifndef CHESSMAN_H
#define CHESSMAN_H

#include <QString>
#include <QDataStream>

enum class Colour
{
    WHITE = 0
    , BLACK
    , EMPTY

    , COUNT
};

struct Chessman {
    enum type
    {
        //Warning! If the order is changed - the chessman_name array
        //initialization should be updated
        EMPTY = 0
        , KING
        , QUEEN
        , ROOK
        , BISHOP
        , KNIGHT
        , PAWN

        , COUNT
    } title;

    Colour colour;
};

QDataStream& operator<<( QDataStream& out, const Chessman& chessman );
QDataStream& operator>>( QDataStream& in, Chessman& chessman );

static QString chessman_name[] = {
    QString{" "}
    , QString{"K"}
    , QString{"Q"}
    , QString{"R"}
    , QString{"B"}
    , QString{"Kt"}
    , QString{"P"}
};

inline Colour next_player( const Colour current_player )
{
    return current_player == Colour::WHITE ?
                Colour::BLACK : Colour::WHITE;
}

#endif // CHESSMAN_H
