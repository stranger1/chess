#ifndef CHESSHISTORY_H
#define CHESSHISTORY_H

#include "chessman.h"
#include "chess_move.h"
#include "chessgrid.h"

#include <QList>
#include <QDataStream>

#include <memory>

class ChessHistory
{
public:
    ChessHistory();
    ChessHistory( const ChessGrid& input_board, const QList<ChessMove> input_moves );

    void set_initial_board( const ChessGrid& chessboard );
    void add_move( const ChessMove& move );
    std::shared_ptr<ChessMove> next_move();
    std::shared_ptr<ChessMove> prev_move();
    bool is_last() const;
    bool is_first() const;
    void clear_history();

    ChessGrid initial_board() const;
    QList<ChessMove> moves() const;

private:
    ChessGrid m_initial_board;
    QList<ChessMove> m_moves;
    QList<ChessMove>::const_iterator m_current_move;
};

QDataStream& operator<<( QDataStream& out, const ChessHistory& chessboard );
/*! \param[out] chessboard deserialised object */
QDataStream& operator>>( QDataStream& in, ChessHistory& chessboard );

#endif // CHESSHISTORY_H
