#ifndef CHESSGRID_H
#define CHESSGRID_H

#include "chessman.h"

#include <QPair>

//TODO: Make it an actual class sometime. For now it is just a typedef of STL vector
//for easier serialisation implementation
typedef std::vector<Chessman> ChessGrid;

static constexpr int GRID_DIMENSION = 8;

/*! \return Chessman previously located on this cell */
Chessman set_cell( ChessGrid& chessgrid, const int letter, const int number, Chessman chessman );
int coord_to_index( const int letter, const int number );
/*! \return Pair <number, letter> */
QPair<int, int> index_to_coord( const int index );
int delta_number( const int from_index, const int to_index );
int delta_letter( const int from_index, const int to_index );

enum class ChessSetup
{
    EMPTY
    , STANDARD
};

ChessGrid create_chessgrid( ChessSetup setup = ChessSetup::EMPTY );

#endif // CHESSGRID_H
